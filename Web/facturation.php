<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once("action/FacturationAction.php");
	$action = new FacturationAction();
	$action->execute();
	
	require_once("partial/header.php");

	if(!isset($_GET["ajouter"]) && !isset($_GET["payer"])){
?>

	<div class ="choixFacture">
		<div><h2>Veuillez choisir une des option suivante :</h2>
		<form action="facturation.php" method="get">
			<input class="bouton" type="submit" name="ajouter" value="Ajouter une facture" /><br/>
			<input class="bouton" type="submit" name="payer" value="Payer une facture" /><br/>
		</form>
	</div>

<?php
	}

	else if(isset($_GET["ajouter"])){
		$result=$action->getListeClients();
		?>
		
		<div class="formulaire ib ">
			<div>
				<h2>Pour quel client voulez-vous ajouter une facture :</h2>
			</div>
			<form class="client" action = "facturation.php" method="post">
				<fieldset>
					<?php
						foreach($result as $client){
					?>	
						<div>
							<label class="ib left"><?php echo ($client["prenom"]." ".$client["nom"]." (".$client["compagnie"].")");?> </label>
							<input class="right button bouton" type="submit" name="<?php echo $client["id"] ?>" value="Ajouter" />
						</div>	
						<div class=" clear"></div>
					<?php
						}
					?>
				</fieldset>
			</form>	
		</div>


<?php
	}

	elseif(isset($_GET["payer"])){
		$result=$action->getListeClients();
		?>
		
		<div class="formulaire ib">
			<div>
				<h2>Veuillez Selectionner le client :</h2>
			</div>
				<form class="client" action = "facturation.php" method="GET">
					<fieldset>
						<?php
							foreach($result as $client){
							?>	
						<div>
							<label class="ib left">
								<?php 
									echo $client["prenom"]." ".$client["nom"]." (".$client["compagnie"].")";
								?>
							</label>
							<input type="submit" class="button bouton right" name="<?php echo $client["id"] ?>" value="Voir les Factures" />
						</div>
						<div class="clear"></div>
						<?php
						}
						?>
					</fieldset>	
				</form>			
		</div>

<?php
	}


require_once("partial/footer.php");