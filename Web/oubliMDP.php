<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once("action/oubliMDPAction.php");
	$action = new OubliMDPAction();
	$action->execute();
	require_once("partial/header.php");
?>

	<div class="login ib">
		<form action="oubliMDP.php" method="get">
			<label>Entrez votre adresse Email : </label>
			<input class="block" type="text" name="email" />
			<input class="block" type="submit" value="submit" />
		</form>
	</div>
<?php

require_once("partial/footer.php");