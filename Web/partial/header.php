<?php
/* ---------------------------------------------------
 *                                                    *
 *    Projet synthèse : H2013                         *
 *    Fait Par :    Nicolas Waucheul                  *
 *                  Marc Paquin                       *
 *--------------------------------------------------- */
?>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Facturation</title> 
        <meta http-equiv=Content-Type content="text/html; charset=utf-8" />
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/javascript.js"></script>
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
        <link href="css/header.css" rel="stylesheet" type="text/css"/>
        <link href="css/facturation_index.css" rel="stylesheet" type="text/css"/>
        <link href="css/facturation_client.css" rel="stylesheet" type="text/css"/>
        <link href="css/facturation_pageFac.css" rel="stylesheet" type="text/css"/>
    </head>
    <body OnLoad="document.loginForm.login.focus();">

    	<div class="header">
    		<div class="left ib">
    			<img src="./images/logo.jpg" width=100 height=100 alt="page d'accueil" />
    		</div>

    		<div class="titre ib left">
    			<h2>Systeme de facturation</h2>
    		</div>

    		<div class="navigation ib right middle">
    			 <ul>
                    <?php
                        if($action->isLoggedIn()){
                            ?>
        		            <li>
        		              <a href="accueil.php">Accueil</a>
        		            </li>
        		            <li>
        		              <a href="facturation.php">Facturation</a>
        		            </li>
        		            <li>
        		              <a href="rapports.php">Rapports</a>
        		            </li>
        		            <li>
        		              <a href="clients.php">Clients</a>
        		            </li>
        		            <li>
        		              <a href="profil.php">Profil</a>
        		            </li>
                            <li> [<a href="?logout=true">X</a>]
                            </li>
                            <?php
                        }
                    ?>
         	 	</ul>
        	</div>
    	</div>
        <div class="clear"></div>

    	<div class="container">