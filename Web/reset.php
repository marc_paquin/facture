<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once("action/IndexAction.php");
	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>


<div class="login ib">
	
	<form action="index.php" method="post">
		<label>Nouveau mot de passe temporaire:</label>
		<input class="block" type="text" name="temp" />
		<label>Entrez votre nouveau mot de passe :</label>
		<input class="block" type="password" name="reset" />
		<label>Retappez votre nouveau mot de passe :</label>
		<input class="block" type="password" name="reset2" />
		<input class="block" type="submit" value="submit" /><br/>


	</form>

</div>

<?php

require_once("partial/footer.php");