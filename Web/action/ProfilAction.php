<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once('CommonAction.php');
	
	class ProfilAction extends CommonAction {
		
			public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_MEMBER);
			}
			
			protected function executeAction() {
				if(isset($_POST["nomModif"])){
					UserModele::clientMajNom($_POST["nomModif"]);
					header('location:profil.php');
					exit;
				}
				if(isset($_POST["prenomModif"])){
					UserModele::clientMajPrenom($_POST["prenomModif"]);
					header('location:profil.php');
					exit;
				}
				if(isset($_POST["userNameModif"])){
					UserModele::clientMajUsername($_POST["userNameModif"]);
					header('location:profil.php');
					exit;
				}
				if(isset($_POST["passwordModif"])){
					if($_POST["passwordModif"]===$_POST["passwordConfirm"]){
						UserModele::clientMajPassword($_POST["passwordModif"]);
						header('location:profil.php');
					exit;
					}
					else{
						echo ("Les mots de passe ne sont pas identiques.");
					}
				}
				if(isset($_POST["emailModif"])){
					UserModele::clientMajEmail($_POST["email"]);
					header('location:profil.php');
					exit;
				}
								
			}

		}