<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once('CommonAction.php');
	


	class AccueilAction extends CommonAction {


		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {

		}

		public function getNbrFactures(){
			return ClientModele::getNoFacture();
		}
		public function getNbrFacturesNP(){
			return ClientModele::getNoFactureNP();
		}
		public function getCash(){
			return ClientModele::getCash();
		}
	}