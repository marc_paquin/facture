<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once('CommonAction.php');
	
	class FacturationAction extends CommonAction {
		private $listeClients;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}
		
		protected function executeAction() {
			$this->listeClients = UserModele::listeClients();
			$this->listeClients = json_decode($this->listeClients, true);
			

			if(!empty($_POST)){

				$this->listeClients = UserModele::listeClients();
				$this->listeClients = json_decode($this->listeClients, true);
			
				foreach ($this->listeClients as $client) {

					$temp = $client["id"];
					if(isset($_POST[$temp])){
						header("location:ajouterFacture.php?id=".$temp);
						exit;
						

					}
				}
			}

			if(isset($_POST["ajouterFacture"])){

				echo ClientModele::ajouterFacture();

			}

			if(!empty($_GET)){

				$this->listeClients = UserModele::listeClients();
				$this->listeClients = json_decode($this->listeClients, true);
			
				foreach ($this->listeClients as $client) {

					$temp = $client["id"];
					if(isset($_GET[$temp])){
						header("location:payerFacture.php?id=".$temp);
						exit;
						
					}
				}


			}

			


		}

		public function getListeClients() {
			return $this->listeClients;
			
		}

	}