<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once('CommonAction.php');
	


	class ModifierAction extends CommonAction {
		private $client;
		

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			if(isset($_GET["id"])){
				$_SESSION["idClient"] = $_GET["id"];
				$this->client = UserModele::getClient($_GET["id"]);
				

			}
			
		}

		public function getClient(){
			return $this->client;
		}
	}