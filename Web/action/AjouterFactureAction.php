<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once('CommonAction.php');
	
	class AjouterFactureAction extends CommonAction {

		private $facture;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}
		
		protected function executeAction() {
			
			if(isset($_GET["id"])){
				$_SESSION["client"]=UserModele::getClient($_GET["id"]);
			}

			
			

			if(isset($_GET["ajoutService"])){
				$id=$_SESSION["client"];
				ClientModele::ajouterService($id["id"],$_GET["quantite"], $_GET["service"]);
			}
		}


		public function getServices(){
			return ClientModele::getServices();
		}
	}