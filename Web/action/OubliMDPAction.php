<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once('CommonAction.php');
	require_once('Modele/UserModele.php');
	
	class OubliMDPAction extends CommonAction {
	
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}
		
		protected function executeAction() {
			

			if(isset($_GET["email"])){
				$mail = UserModele::getEmail($_GET["email"]);
				if($mail["email"] === $_GET["email"]){
					$MDPtemp = rand(1000,9999);
					$_SESSION["email"]= $_GET["email"];
					$mail = UserModele::resetPWD($mail["email"], $MDPtemp);
					//methode pour envoyer un mail
					mail($_GET["email"], "Oubli de Mot de passe", "Votre mot de passe temporaire est ".$MDPtemp.", veuillez accéder à ce lien : http://localhost:82/reset.php");
					header("location:OubliMDP.php");
					exit();
				}
				else{
					echo "nimporte quoi";
				}
				
				
			}
		}
		
		
		//Fonction pour nettoyer les champs du formulaire.  Prévient l'injection SQL
		function clean($str) {
			$str = @trim($str);
			if(get_magic_quotes_gpc()) {
				$str = stripslashes($str);
			}
			return PDO::prepare($str);
		}
		
		/*//Nettoyage des valeurs POST
		$username = clean($_POST['username']);
		$password = clean($_POST['password']);
		
		echo $_POST['username'].'<br/><br/>';
		echo $_POST['password'].'<br/><br/>';*/
	}
?>