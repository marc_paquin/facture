<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	class Connection {
		private static $connection;
	
		public static function getConnection() {
			if (!isset(Connection::$connection)) {
				Connection::$connection = new PDO(DB_HOST, DB_USER, DB_PASSWORD);
			}
			
			return Connection::$connection;
		}
	
	
		public static function closeConnection($connection) {
			if (isset(Connection::$connection)) {
				$connection->close();
				Connection::$connection = null;
			}
		}
	
	
	
	
	
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	