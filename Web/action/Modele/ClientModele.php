<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

class ClientModele {	

	public static function getNoFacture() {
		$connection = Connection::getConnection();
		
		$query = $connection->prepare('SELECT COUNT(ID) FROM FACTURE');
		$query->execute();
		$result=$query->fetch();
		return $result[0];
		

	}
	public static function getNoFactureNP() {
		$connection = Connection::getConnection();
		$paye="non";
		$query = $connection->prepare('SELECT COUNT(ID) FROM FACTURE WHERE PAYE=?');
		$query->bindParam(1,$paye);
		$query->execute();
		$result=$query->fetch();
		return $result[0];
		
	}

	public static function getCash() {
		$connection = Connection::getConnection();
		$paye="non";
		$query = $connection->prepare('SELECT SUM(TOTAL) FROM FACTURE WHERE PAYE=?');
		$query->bindParam(1,$paye);
		$query->execute();
		$result=$query->fetch();

		var_dump($result[0]);

		return $result[0];
		
	}
	public static function getNoNextFacture() {
		$connection = Connection::getConnection();
		
		$query = $connection->prepare('SELECT MAX(ID) FROM FACTURE');
		$query->execute();
		$result=$query->fetch();
		return $result[0]+1;
		

	}

	public static function getFacture($id){
		$connection = Connection::getConnection();
		$query = $connection->prepare('SELECT * FROM FACTURE where CLIENT_ID=?');
		$query->bindParam(1,$id);
		$query->execute();
		
		$facture=array();
		while ($row = $query->fetch()) {
			$factureTemp = array();
			$factureTemp["id"] = $row["ID"];
			$factureTemp["date_fact"] = $row["DATE_FACT"];
			$factureTemp["total"] = $row["TOTAL"];
			$factureTemp["paye"]=$row["PAYE"];
			$facture[]=$factureTemp;
		}


		return $facture;

	}

	public static function getFactureBetween($id,$date1,$date2){
		
		$connection = Connection::getConnection();
		$query = $connection->prepare("SELECT * FROM FACTURE where CLIENT_ID=? AND DATE_FACT BETWEEN  to_date(?,'yy-mm-dd') AND to_date(?,'yy-mm-dd')");
		$query->bindParam(1,$id);
		$query->bindParam(2,$date1);
		$query->bindParam(3,$date2);
		$query->execute();
		$facture=array();
		while ($row = $query->fetch()) {
			$factureTemp = array();
			$factureTemp["id"] = $row["ID"];
			$factureTemp["date_fact"] = $row["DATE_FACT"];
			$factureTemp["total"] = $row["TOTAL"];
			$factureTemp["paye"]=$row["PAYE"];
			$facture[]=$factureTemp;
		}


		return $facture;

	}



	public static function verifierPrix($id){
		$connection = Connection::getConnection();
		
		$id = intVal($id);

		$query = $connection->prepare('SELECT * FROM SERVICES where ID=?');
		$query->bindParam(1,$id);
		$query->execute();
		$leService = $query->fetch();
		$resultat=array();
		$resultat["prixUnitaire"]= $leService["COUT_UNITAIRE"];
		return json_encode($resultat);


	}

	public static function ajouterService($client,$quantite,$idService){

		$connection = Connection::getConnection();
		$nextVal = ClientModele::getNoNextFacture();
		$query = $connection->prepare('INSERT INTO SERVICES_CLIENTS values(SERVICE_CLIENT_SEQ.NEXTVAL,?,?,?,?)');
		$query->bindParam(1,$nextVal);
		$query->bindParam(2,$client);
		$query->bindParam(3,$idService);
		$query->bindParam(4,$quantite);
		$query->execute();


	}

	public static function getServices(){
		$connection = Connection::getConnection();
		$nextVal = ClientModele::getNoNextFacture();
		$query = $connection->prepare('SELECT * FROM SERVICES_CLIENTS WHERE NO_FACTURE = ?');
		$query->bindParam(1,$nextVal);
		$query->execute();

		$servicesTemp=array();
		$services=array();
		while($row=$query->fetch()){
			$servicesTemp["quantite"]=$row["QUANTITE"];
			$servicesTemp["client"]=$row["CLIENT_ID"];
			$servicesTemp["service"]=$row["NO_SERVICE"];
			$services[]=$servicesTemp;

		}

		$query2 = $connection->prepare('SELECT * FROM SERVICES');
		$query2->execute();

		$descServicesTemp=array();
		$descServices=array();
		while($row2=$query2->fetch()){
			$descServicesTemp["noService"]=$row2["ID"];
			$descServicesTemp["desc"]=$row2["SERVICE"];
			$descServicesTemp["cout"]=$row2["COUT_UNITAIRE"];
			$descServices[]=$descServicesTemp;
		}

		
		for ($j=0; $j < sizeof($services); $j++) { 
			for ($i=0; $i < sizeof($descServices); $i++) {
				if($services[$j]["service"]===$descServices[$i]["noService"]){
					$services[$j]["service"]=$descServices[$i]["desc"];
					$services[$j]["cout"]=$descServices[$i]["cout"];
				}
			}
		}
		return $services;

	}

	public static function ajouterFacture(){
		
		$id=$_SESSION["client"];
		$prix=$_SESSION["prixTotal"];
		$prix=intVal($prix);
		$id["id"] = intval($id["id"]);
		$paye="non";
		$nextVal = ClientModele::getNoNextFacture();
		$connection = Connection::getConnection();
		$query = $connection->prepare('INSERT INTO FACTURE values(?,?,SYSDATE,?,?)');
		$query->bindParam(1,$nextVal);
		$query->bindParam(2,$id["id"]);
		$query->bindParam(3,$prix);
		$query->bindParam(4,$paye);

		$query->execute();
	}

	public static function payerFacture($id){
		
		
		$connection = Connection::getConnection();
		$paye="oui";
		$query = $connection->prepare('UPDATE FACTURE SET PAYE=? WHERE ID=?');
		$query->bindParam(1,$paye);
		$query->bindParam(2,$id);
		$query->execute();
	}
	

}



