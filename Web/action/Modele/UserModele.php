<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	class UserModele {	
	
		public static function authenticate($username, $password) {
			$userInfo = null;
			$utilisateur = null;
			$result=null;
			$compteur=null;
			$connection = Connection::getConnection();
			$query = $connection->prepare('SELECT * FROM USERS WHERE USERNAME = ?');
			$query->bindParam(1,$username);
			$query->execute();
			
			$temp=$query->fetch();

			if($temp == null){
				header('location:index.php?wrongUser=1');
				exit;
			}
			
			else{
				
				$compteur = $temp["ESSAIS"];				
			}
			
			$query1 = $connection->prepare("SELECT to_Char(TIMELEFT, 'SSSSS'),to_Char(SYSDATE, 'SSSSS') FROM USERS WHERE USERNAME = ?");
			$query1->bindParam(1,$username);
			$query1->execute();
			$temp=$query1->fetch();
			
			if($temp[0] > $temp[1]){
					header('location:index.php?banni=1');
					exit;
			}

			else{

				$password = sha1($password . "pokemon");
				
				$query = $connection->prepare('SELECT * FROM USERS WHERE USERNAME = ? AND PASSWORD = ?');
				$query-> bindParam(1,$username);
				$query-> bindParam(2,$password);
				$query->execute();
				$temp=$query->fetch();

				if($temp==null && ($compteur>=1)){
					$compteur=$compteur-1;
					$query2 = $connection->prepare("UPDATE USERS SET ESSAIS = ? WHERE USERNAME =?");
					$query2-> bindParam(1,$compteur);
					$query2-> bindParam(2,$username);
					$query2->execute();

				}

				else if(($temp==null) && ($compteur<1)){

					$query2 = $connection->prepare("UPDATE USERS SET TIMELEFT= TO_DATE(TO_CHAR(SYSDATE+(300/86400),'yy-mm-dd,HH24:MI:SS'),'yy-mm-dd,HH24:MI:SS'), ESSAIS=5 WHERE USERNAME =?");
					$query2-> bindParam(1,$username);
					$query2->execute();

				}
				if($temp!=null){
					$temp2=array();
					$_SESSION["user"]=$temp;
					
					$temp2["visibility"]=$temp["VISIBILITY"];
					$temp2["username"]=$temp["USERNAME"];
					return $temp2;
				}
			}

		}

		public static function getEmail($email){

			$connection = Connection::getConnection();

			$query = $connection->prepare('SELECT EMAIL FROM USERS WHERE EMAIL =?');
			$query->bindParam(1,$email);
			$query->execute();

			$mail = null;
			while($row = $query->fetch()){
				$mail = array();
				$mail["email"]=$row["EMAIL"];
			}

			return $mail;

		}

		public static function resetPWD($mail,$pwd){

			$pwd = sha1($pwd . "pokemon");

			$connection = Connection::getConnection();
			$query = $connection->prepare('UPDATE USERS SET PASSWORD=? WHERE EMAIL =?');
			$query->bindParam(1,$pwd);
			$query->bindParam(2,$mail);
			$query->execute();

			$mail = null;
			while($row = $query->fetch()){
				$mail = array();
				$mail["email"]=$row["EMAIL"];
				$mail["pwd"]=$row["PASSWORD"];
			}

			return $mail;
		}

		public static function checkPWD($pwd){

			$pwd = sha1($pwd . "pokemon");
			$connection = Connection::getConnection();
			$query = $connection->prepare('SELECT PASSWORD FROM USERS WHERE PASSWORD =?');
			$query->bindParam(1,$pwd);
			$query->execute();

			
			$retour = null;
			while($row = $query->fetch()){
				$retour = array();
				$retour["pwd"]=$row["PASSWORD"];
			}

			return $retour;
		}

		public static function ajouterClient($prenom, $nom, $compagnie, $numCivique, $rue, $ville, $province, $codePostal, $telephone, $fax, $courriel, $commentaire){

			$connection = Connection::getConnection();
			$query = $connection->prepare('INSERT INTO CLIENT (ID,PRENOM,NOM,NOM_COMPAGNIE,NUM_CIVIQUE,RUE,VILLE,PROVINCE,CODE_POSTAL,NUM_TEL,NUM_FAX,EMAIL,VISIBILITY,COMMENTAIRE) VALUES (FAC_CLI_ID_SEQ.nextVal,?,?,?,?,?,?,?,?,?,?,?,?,?)');
			//$query->bindParam(1,$ID, PDO::PARAM_INT);
			//$ID=1;

			$query->bindParam(1,$prenom);
			$query->bindParam(2,$nom);
			$query->bindParam(3,$compagnie);
			$query->bindParam(4,$numCivique);
			$query->bindParam(5,$rue);
			$query->bindParam(6,$ville);
			$query->bindParam(7,$province);
			$query->bindParam(8,$codePostal);
			$query->bindParam(9,$telephone);
			$query->bindParam(10,$fax);
			$query->bindParam(11,$courriel);
			$query->bindParam(12,$visibility, PDO::PARAM_INT);
			$visibility=1;
			$query->bindParam(13,$commentaire);
			$query->execute();
			echo "Query execute";

		}

		public static function modifierClient($id,$prenom, $nom, $compagnie, $numCivique, $rue, $ville, $province, $codePostal, $telephone, $fax, $courriel, $commentaire){

			$connection = Connection::getConnection();
			$query = $connection->prepare('UPDATE CLIENT SET PRENOM=?,NOM=?,NOM_COMPAGNIE=?,NUM_CIVIQUE=?,RUE=?,VILLE=?,PROVINCE=?,CODE_POSTAL=?,NUM_TEL=?,NUM_FAX=?,EMAIL=?,COMMENTAIRE=? WHERE ID=?');
			
			$query->bindParam(1,$prenom);
			$query->bindParam(2,$nom);
			$query->bindParam(3,$compagnie);
			$query->bindParam(4,$numCivique);
			$query->bindParam(5,$rue);
			$query->bindParam(6,$ville);
			$query->bindParam(7,$province);
			$query->bindParam(8,$codePostal);
			$query->bindParam(9,$telephone);
			$query->bindParam(10,$fax);
			$query->bindParam(11,$courriel);
			$query->bindParam(12,$commentaire);
			$query->bindParam(13,$id);

			$query->execute();
	

			$query->execute();
		}

		public static function listeClients(){

			$connection = Connection::getConnection();
			$query = $connection->prepare('SELECT id,prenom,nom,nom_compagnie FROM CLIENT');
			$query->execute();

			$retour =array();

			while($row = $query->fetch()){
				$compagnie = array();
				$compagnie["prenom"] = $row["PRENOM"];
				$compagnie["nom"] = $row["NOM"];
				$compagnie["compagnie"] = $row["NOM_COMPAGNIE"];
				$compagnie["id"] = $row["ID"];
				$retour[] = $compagnie;
			}
			

			return json_encode($retour);
		}

		public static function getClient($id){
			$connection = Connection::getConnection();
			$query = $connection->prepare('SELECT * FROM CLIENT WHERE ID = ?');
			$query->bindParam(1,$id);

			$query->execute();

			$retour = array();

			if($row = $query->fetch()){
				$retour["id"]=$row["ID"];
				$retour["prenom"] = $row["PRENOM"];
				$retour["nom"] = $row["NOM"];
				$retour["compagnie"] = $row["NOM_COMPAGNIE"];
				$retour["numCivique"] = $row["NUM_CIVIQUE"];
				$retour["rue"] = $row["RUE"];
				$retour["ville"] = $row["VILLE"];
				$retour["province"] = $row["PROVINCE"];
				$retour["codePostal"] = $row["CODE_POSTAL"];
				$retour["numTel"] = $row["NUM_TEL"];
				$retour["numFax"] = $row["NUM_FAX"];
				$retour["email"] = $row["EMAIL"];
				$retour["commentaire"] = $row["COMMENTAIRE"];
			}

			return $retour;
		}

		public static function supprimerClient($id){
			$connection = Connection::getConnection();
			$query = $connection->prepare('DELETE FROM CLIENT WHERE ID=?');
			$query->bindParam(1,$id);

			$query->execute();

		}

		public static function clientMajNom($nom){
			$user=$_SESSION["user"];

			$connection = Connection::getConnection();
			$query = $connection->prepare('UPDATE USERS SET LAST_NAME=? WHERE ID=?');
			$query->bindParam(1,$nom);
			$query->bindParam(2,$user["id"]);

			$query->execute();

			$user["nom"]=$nom;
			$_SESSION["user"]=$user;

		}

		public static function clientMajPrenom($prenom){
			$user=$_SESSION["user"];
			$connection = Connection::getConnection();
			$query = $connection->prepare('UPDATE USERS SET FIRST_NAME=? WHERE ID=?');
			$query->bindParam(1,$prenom);
			$query->bindParam(2,$user["id"]);

			$query->execute();

			$user["prenom"]=$prenom;
			$_SESSION["user"]=$user;

		}

		public static function clientMajUsername($username){
			$user=$_SESSION["user"];
			$connection = Connection::getConnection();
			$query = $connection->prepare('UPDATE USERS SET USERNAME=? WHERE ID=?');
			$query->bindParam(1,$username);
			$query->bindParam(2,$user["id"]);

			$query->execute();

			$user["username"]=$username;
			$_SESSION["user"]=$user;

		}

		public static function clientMajPassword($password){
			$password=sha1($password . "pokemon");

			$user=$_SESSION["user"];
			$connection = Connection::getConnection();
			$query = $connection->prepare('UPDATE USERS SET PASSWORD=? WHERE ID=?');
			$query->bindParam(1,$password);
			$query->bindParam(2,$user["id"]);

			$query->execute();


		}

			public static function clientMajEmail($email){
			

			$user=$_SESSION["user"];
			$connection = Connection::getConnection();
			$query = $connection->prepare('UPDATE USERS SET EMAIL=? WHERE ID=?');
			$query->bindParam(1,$password);
			$query->bindParam(2,$user["id"]);

			$query->execute();

			$user["email"]=$email;
			$_SESSION["user"]=$user;


		}

		

	}