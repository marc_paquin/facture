<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once('CommonAction.php');
	
	class ClientsAction extends CommonAction {

			private $listeClients;

			public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_MEMBER);
			}
			
			protected function executeAction() {

				if(isset($_GET["formAjouter"])){
					UserModele::ajouterClient($_GET["prenom"], $_GET["nom"], $_GET["compagnie"], $_GET["numCivique"], $_GET["rue"], $_GET["ville"], $_GET["province"], $_GET["codePostal"], $_GET["telephone"], $_GET["fax"], $_GET["courriel"], $_GET["formAjouter"]);

				}
				if(isset($_GET["modifier"])){
					$this->listeClients = UserModele::listeClients();
					$this->listeClients = json_decode($this->listeClients, true);
				}
				if(isset($_GET["formMAJ"])){
					UserModele::modifierClient($_SESSION["idClient"],$_GET["prenom"], $_GET["nom"], $_GET["compagnie"], $_GET["numCivique"], $_GET["rue"], $_GET["ville"], $_GET["province"], $_GET["codePostal"], $_GET["telephone"], $_GET["fax"], $_GET["courriel"], $_GET["commentaire"]);
					header("location:clients.php");
					exit;
				}


				elseif(!empty($_POST)){

					$this->listeClients = UserModele::listeClients();
					$this->listeClients = json_decode($this->listeClients, true);
					foreach ($this->listeClients as $client) {
						
						$temp = $client["id"];
						if(isset($_POST[$temp])){
							header("location:modifier.php?id=".$temp);
							exit;
							

						}
					}

				}




				
			}

			public function getListeClients() {
				return $this->listeClients;
				
			}

		}