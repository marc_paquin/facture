<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once('CommonAction.php');	
	require_once('Modele/UserModele.php');	
	
	class indexAction extends CommonAction {
	
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}
		
		protected function executeAction() {
			if(isset($_POST['password']) && isset($_POST['login'])){
				$userInfo = UserModele::authenticate($_POST["login"],$_POST["password"]);

				if(isset($userInfo)){
					$_SESSION["loggedIn"] = $userInfo["visibility"];
					$_SESSION["login"] = $userInfo["username"];

					header("location:accueil.php");
					exit();
				}
			}
			
			if(isset($_GET["wrongUser"])){
				echo("Le nom d'usager est incorrect.");

			}

			if(isset($_POST["reset"])){

				$pwd = UserModele::checkPWD($_POST["temp"]);
				if(sha1($_POST["temp"]) === $pwd["pwd"]){
					if($_POST["reset"] === $_POST["reset2"]){
						UserModele::resetPWD($_SESSION["email"],$_POST["reset"]);
					}
					else{
						header("location:reset.php");
						exit();
					}
				}
			}
			if(isset($_GET["banni"])){
				echo("Le3 compte est vérouillé");

			}
		}


		
		//Fonction pour nettoyer les champs du formulaire.  Prévient l'injection SQL
		function clean($str) {
			$str = @trim($str);
			if(get_magic_quotes_gpc()) {
				$str = stripslashes($str);
			}
			return PDO::prepare($str);
		}
		
		/*//Nettoyage des valeurs POST
		$username = clean($_POST['username']);
		$password = clean($_POST['password']);
		
		echo $_POST['username'].'<br/><br/>';
		echo $_POST['password'].'<br/><br/>';*/
	}
