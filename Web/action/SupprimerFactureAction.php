<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once('CommonAction.php');
	
	class SupprimerFactureAction extends CommonAction {
		private $listeFactures;
		
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}
		
		protected function executeAction() {
			if(isset($_GET["id"])){
				$_SESSION["laListe"]=$_GET["id"];
				$this->listeFactures=ClientModele::getFacture($_GET["id"]);
			}
			
			if(isset($_POST)){
				$this->listeFactures=ClientModele::getFacture($_SESSION["laListe"]);
				foreach ($this->listeFactures as $facture) {

					$temp = $facture["id"];
					if(isset($_POST[$temp])){
						CLientModele::supprimerFacture($temp);
						header("location:facturation.php");
						exit;
						
					}
				}
			}
		}

		public function getListeFactures(){
			return $this->listeFactures;
		}
		

	}