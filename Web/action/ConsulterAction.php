<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synth�se : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once('CommonAction.php');
	


	class ConsulterAction extends CommonAction {
		private $facture;
		private $id;
		

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_MEMBER);
		}

		protected function executeAction() {
			if(!empty($_POST)){
				$this->id = UserModele::listeClients();
				$this->id = json_decode($this->id, true);
				foreach ($this->id as $idClient) {

					$temp = $idClient["id"];
						if(isset($_POST[$temp])){
							
							$_SESSION["idFacture"]=$_POST[$temp];
							if(($_POST["nextDate"]!="YY-MM-DD")){
								$this->facture=ClientModele::getFactureBetween($temp,$_POST["currentDate"],$_POST["nextDate"]);
							}
							else{
								$this->facture=ClientModele::getFacture($temp);
							}
						}
				}
			}

			else{
				$this->facture = CLientModele::getFacture($_SESSION["idFacture"]);
			}
			
		}

		

		public function getFacture(){
			return $this->facture;
		}
	}
