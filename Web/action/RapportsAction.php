<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once('CommonAction.php');
	
	class RapportsAction extends CommonAction {
			
			private $listeClients;

			public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_MEMBER);
			}
			
			protected function executeAction() {
				$this->listeClients = UserModele::listeClients();
				$this->listeClients = json_decode($this->listeClients, true);
			}

			public function getListeClients() {
				return $this->listeClients;
				
			}

		}