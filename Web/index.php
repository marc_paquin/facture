<?php

/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once("action/IndexAction.php");
	$action = new IndexAction();
	$action->execute();

	require_once("partial/header.php");
?>


<div class="login ib" id="login">

	<form action="index.php" method="post" name="loginForm">
		<label>Nom de compte :</label>
		<input class="block" type="text" name="login" />
		<label>Mot de passe :</label>
		<input class="block" type="password" name="password" />
		<input class="block" type="submit" value="submit" /><br/>
		<a href="oubliMDP.php">Mot de passe oublié?</a>

	</form>

</div>

<?php

require_once("partial/footer.php");