<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once("action/ClientsAction.php");
	$action = new ClientsAction();
	$action->execute();
	$result=$action->getListeClients();
	require_once("partial/header.php");


	if(!isset($_GET["ajouter"]) && !isset($_GET["modifier"]) && !isset($_GET["supprimer"])){
?>
	
	<div class ="choixClient">
		<div><h2>Veuillez choisir une des option suivante :</h2>
		<form action="clients.php" method="get">
			<input  class="bouton" type="submit" name="ajouter" value="Ajouter" /><br/>
			<input  class="bouton" type="submit" name="modifier" value="Modifier" /><br/>
			<input  class="bouton" type="submit" name="supprimer" value="Supprimer" /><br/>
		</form>
	</div>

<?php
	}

	else if(isset($_GET["ajouter"])){
		?>
		
		<div class="formulaire ib">
			<div>
				<h2>Veuillez remplir les champs ci-dessous :</h2>
			</div>
			<div>
			<form  class="left form" action="clients.php" method="get">
				<div class="label">
						<label class="important ">Prenom :</label>
						<label class="important ">Nom *:</label>
						<label class="important ">Nom de la compagnie *:</label>
						<label>Numéro civique :</label>
						<label>Rue :</label>
						<label>Ville *:</label>
						<label>Province :</label>
						<label>Code Postal :</label>
						<label>Numero de téléphone :</label>
						<label>Numero de fax :</label>
						<label class="important">Adresse courriel *:</label>
					</div>
					<div class="input">
						<input type="text" name="prenom" value=""/><br/>					
						<input type="text" name="nom" value=""/><br/>					
						<input type="text" name="compagnie" value=""/><br/>					
						<input type="text" name="numCivique" value=""/><br/>					
						<input type="text" name="rue" value=""/><br/>					
						<input type="text" name="ville" value=""/><br/>					
						<input type="text" name="province" value=""/><br/>					
						<input type="text" name="codePostal" value=""/><br/>					
						<input type="text" name="telephone" value=""/><br/>					
						<input type="text" name="fax" value=""/><br/>					
						<input type="text" name="courriel" value=""/><br/>
					</div>	
			</div>
				<div class="ib right">
					<label for="commentaires"> Commentaires: </label>
					<textArea id="commentaires" class="ckeditor" name="commentaire" row:"50" cols="100" style="width:50%; height:20px"></textArea><br/>
					<input type="submit" name="formAjouter" value="Valider" /><br/>
				</div>
			</form>
		</div>


<?php
	}

	else if(isset($_GET["modifier"])){
		
		?>
		
		<div class="formulaire ib">
			<div>
				<h2>Veuillez sélectionner le client à modifier :</h2>
			</div>
				<form class="table" action = "clients.php" method="post">
				<?php
					foreach($result as $value){
					?>	<div>
							<label class="ib left">
								<?php 
									echo $value["prenom"]. " " .$value["nom"]. " (" .$value["compagnie"].")";
								?>
							</label>
							<input class="button bouton right" type="submit" name="<?php echo $value["id"] ?>" value="Modifier" />
						</div>
						<div class="clear"></div>

					<?php
					}
					?>
				</form>

		</div>

<?php
	}


	else if(isset($_GET["supprimer"])){
		header("location:supprimer.php?supprimer=Supprimer");
		exit;
	}

		


require_once("partial/footer.php");