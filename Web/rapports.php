<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once("action/RapportsAction.php");
	$action = new RapportsAction();
	$action->execute();
	$result=$action->getListeClients();
	require_once("partial/header.php");
?>


	<div class="formulaire ib">
			<div>
				<h2>Veuillez sélectionner le client à consulter :</h2>
			</div>
				<form class="consulter" action = "consulter.php" method="POST">
					<div>
						<h3>Choisissez deux dates :</h3>
						<label>De :</labeL>
						<input type="text" name="currentDate" value="<?php echo date('y-m-d') ?>">
						<label> A :</labeL>
						<input type="text" name="nextDate" value="YY-MM-DD">
					</div>
				<?php
					foreach($result as $value){
					?>	<div>
							<label class="ib left">
								<?php 
									echo $value["prenom"]. " " .$value["nom"]. " (" .$value["compagnie"].")";
								?>
							</label>
							<input class="button bouton right" type="submit" name="<?php echo $value["id"] ?>" value="Consulter" />
						</div>
						<div class="clear"></div>

					<?php
					}
					?>
				</form>

		</div>


<?php

require_once("partial/footer.php");