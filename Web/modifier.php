<?php

/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once("action/ModifierAction.php");
	$action = new ModifierAction();
	$action->execute();
	$client = $action->getClient();

	require_once("partial/header.php");
?>


<div class="formulaire ib">
	<div>
		<h2>Veuillez remplir les champs à modifier :</h2>
	</div>
	<div>
		<form class="left form" action="clients.php" method="GET">
			<div class="label">
				<label class="important ">Prenom :</label>
				<label class="important ">Nom *:</label>
				<label class="important ">Nom de la compagnie *:</label>
				<label>Numéro civique :</label>
				<label>Rue :</label>
				<label>Ville *:</label>
				<label>Province :</label>
				<label>Code Postal :</label>
				<label>Numero de téléphone :</label>
				<label>Numero de fax :</label>
				<label class="important">Adresse courriel *:</label>
			</div>
			<div class="input">
				<input type="text" name="prenom" value="<?php echo $client["prenom"]?>"/><br/>					
				<input type="text" name="nom" value="<?php echo $client["nom"]?>"/><br/>					
				<input type="text" name="compagnie" value="<?php echo $client["compagnie"]?>"/><br/>					
				<input type="text" name="numCivique" value="<?php echo $client["numCivique"]?>"/><br/>					
				<input type="text" name="rue" value="<?php echo $client["rue"]?>"/><br/>					
				<input type="text" name="ville" value="<?php echo $client["ville"]?>"/><br/>					
				<input type="text" name="province" value="<?php echo $client["province"]?>"/><br/>					
				<input type="text" name="codePostal" value="<?php echo $client["codePostal"]?>"/><br/>					
				<input type="text" name="telephone" value="<?php echo $client["numTel"]?>"/><br/>					
				<input type="text" name="fax" value="<?php echo $client["numFax"]?>"/><br/>					
				<input type="text" name="courriel" value="<?php echo $client["email"]?>"/><br/>
			</div>	
	</div>
			<div class="ib right">
				<label for="commentaires"> Commentaires: </label>
				<textArea id="commentaires" class="ckeditor" name="commentaire" row:"50" cols="100" style="width:50%; height:20px" value=<?php echo $client["commentaire"]?>></textArea><br/>
				<input type="submit" name="formMAJ" value="Mettre à jour" /><br/>
			</div>	
		</form>
</div>

<?php

require_once("partial/footer.php");