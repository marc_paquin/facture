<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once("action/SupprimerAction.php");
	$action = new SupprimerAction();
	$action->execute();
	$result=$action->getListeClients();
	require_once("partial/header.php");



?>

	<div class="formulaire ib">
		<div>
			<h2>Veuillez sélectionner le client à supprimer :</h2>
		</div>
			<form class="client" action = "supprimer.php" method="post">
			<?php
				foreach($result as $value){
				?>	<div>
						<label class="ib left">
							<?php 
								echo $value["prenom"]. " " .$value["nom"]. " (" .$value["compagnie"].")";
							?>
						</label>
						<input class="ib bouton right icon" type="submit" name="<?php echo $value["id"] ?>" value="SupprimerClient" />
					</div>
					<div class="clear"></div>

				<?php
				}
				?>
			</form>

	</div>

<?php

	require_once("partial/footer.php");