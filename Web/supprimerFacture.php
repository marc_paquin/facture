<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */


	require_once("action/SupprimerFactureAction.php");
	$action = new SupprimerFactureAction();
	$action->execute();
	$listeFacture=$action->getListeFactures();
	

	require_once("partial/header.php");

?>


<div class="formulaire ib">
	<div>

		<h2>Selectionnez la facture a supprimer: </h2>

		<div></div>

		<table style="margin:auto;">
		  <tr>
		    <th># de la Facture</th>
		    <th>Date de Facturation</th>
		    <th>Cout Total</th>
		  </tr>
			<?php
			for ($i=0; $i < sizeof($listeFacture) ; $i++) {
				$listeFacture[$i]=(array)$listeFacture[$i];
				?>
				<tr>
		  			<td><?php echo ($listeFacture[$i]["id"]) ?></td>
		  			<td><?php echo ($listeFacture[$i]["date_fact"]) ?></td>
		  			<td><?php echo ($listeFacture[$i]["total"]) ?> $</td>
	  			
	  			<td>
	  				<form action="supprimerFacture.php" method="POST">
					<input type="submit" name="<?php echo ($listeFacture[$i]["id"]) ?>" value="Supprimer la Facture">
					</form>
				</td>
			</tr>
				<?php
			}
			?>
		  
		</table>


	</div>
	

<?php
	
require_once("partial/footer.php");
