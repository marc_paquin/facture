<?php
/* ---------------------------------------------------
 *					                                  *
 *    Projet synthèse : H2013		                  *
 *    Fait Par : 	Nicolas Waucheul			      *
 *					Marc Paquin                   	  *
 *--------------------------------------------------- */

	require_once("action/ProfilAction.php");
	$action = new ProfilAction();
	$action->execute();
	require_once("partial/header.php");
	$user=$_SESSION["user"];
?>

	<h2>Profil</h2>

	<div class="center">
		<h3>Cliquez sur le champ à modifier</h3>

		<form id="profil" action="profil.php" method="POST">

			<?php if (isset($_POST["nom"])){
					?>
					<label>Nom: </label>
					<input class="boutonProfil" type="text" name="nomModif" value="<?php echo $user['nom'] ?>" />
					<?php
				}
				else{
				?>
					<label>Nom: </label>
					<input class="boutonProfil" type="submit" name="nom" value="<?php echo $user['nom'] ?>" />
				<?php
				}
			?>
			<div></div>

			<?php if (isset($_POST["prenom"])){
					?>
					<label>Prenom: </label>
					<input class="boutonProfil" type="text" name="prenomModif" value="<?php echo $user['prenom'] ?>" />
					<?php
				}
				else{
				?>
					<label>Prenom: </label>
					<input class="boutonProfil" type="submit" name="prenom" value="<?php echo $user['prenom'] ?>" />
				<?php
				}
			?>
			<div></div>
			<?php if (isset($_POST["userName"])){
					?>
					<label>Username: </label>
					<input class="boutonProfil" type="text" name="userNameModif" value="<?php echo $user['username'] ?>" />
					<?php
				}
				else{
				?>
					<label>Username: </label>
					<input class="boutonProfil" type="submit" name="userName" value="<?php echo $user['username'] ?>" />
				<?php
				}
			?>
			<div></div>
			<?php if (isset($_POST["password"])){
					?>	
					<label>Mot de passe: </label>
					<input class="boutonProfil" type="password" name="passwordModif" value="Mot de passe" />
					<label>Confirmer mot de passe: </label>
					<input class="boutonProfil" type="password" name="passwordConfirm" value="Mot de passe" />
				<?php
				}
				else{
				?>	
					<label>Mot de passe: </label>
					<input class="boutonProfil" type="submit" name="password" value="Mot de passe" />
				<?php
				}
			?>
			<div></div>
			<?php if (isset($_POST["email"])){
					?>	
					<label>Adresse Email: </label>
					<input class="boutonProfil" type="text" name="emailModif" value="<?php echo $user['email'] ?>" />
				<?php
				}
				else{
				?>	
					<label>Adresse Email: </label>
					<input class="boutonProfil" type="submit" name="email" value="<?php echo $user['email'] ?>" />
				<?php
				}
			?>






		</form>





	</div>


<?php

require_once("partial/footer.php");