/*----------------------------------------------------------------
*                                                                *
*                   Projet synth�se : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/

window.onload = init;


function init(){

	
}

function profilListener(){
	$("#infosProfil a").click(function(event) {
  		event.preventDefault();
  		$(event.target).parent().next().css("display", "block");
  		$(event.target).parent().css("display", "none");
  	} );
}

function removeClient(element, id){
	$.ajax({
		type: "POST",
		url: "deleteClient.php",
		data:"id="+id,
		success: function(){
			window.location.reload();
		}
	});
}

function modifyClient(element, id){
	var row = $(element).parent().children();
	var tempNom = row[0].innerHTML;
	var tempPrenom = row[1].innerHTML;
	var tempAdresse = row[2].innerHTML;
	var date = new Date();
	if(date.getMonth.length < 2){
		var dateCourante = date.getDate() + "/0" + (date.getMonth()+1) + "/" + date.getFullYear();
	}
	else {
		var dateCourante = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear();
	}
	$(row[0]).replaceWith("<td><input id='modifyLastName' class='modifyInput'type='text' name='modifyLastName' /></td>");
	$(row[1]).replaceWith("<td><input id='modifyFirstName'class='modifyInput'type='text' name='modifyFirstName' /></td>");
	$(row[2]).replaceWith("<td><input id='modifyAddress' class='modifyInput'type='text' name='modifyAddress' /></td>");
	$(row[3]).replaceWith("<td><input id='modifyLastModify' class='modifyInput'type='text' name='modifyLastModify' /></td>");
	$(row[4]).replaceWith("<td id='tdVide'></td>");
	$(row[5]).replaceWith("<td id='tdOK' onclick='confirmModify("+id+")' id='modifyLastName' id='bOK' class='button'><input type='image' src='images/boutonOK.png' name='bOK'/></td>");
	document.getElementById("modifyLastName").value = tempNom;
	document.getElementById("modifyFirstName").value = tempPrenom;
	document.getElementById("modifyAddress").value = tempAdresse;
	document.getElementById("modifyLastModify").value = dateCourante;
	$(".button").css("display", "none");
	$("#tdOK").css("display","block");
	$("#bOK").css("display","block");
}

function confirmModify(id){
	var nom = document.getElementById("modifyLastName").value;
	var prenom = document.getElementById("modifyFirstName").value;
	var adresse = document.getElementById("modifyAddress").value;
	var last = document.getElementById("modifyLastModify").value;
	$.ajax({
		type: "POST",
		url: "modifyClient.php",
		data:{id:id, nom:nom, prenom:prenom, adresse:adresse, last:last},
		success: function(){
			window.location.reload();
		}
	});
}

function showFactures(element)
{
	var id = element.id;
	console.log(id);
	$.ajax({
		type:"post",
		url:"showFactures.php",
		data:"id="+id,
		success:function(factures){
			afficherFactures(factures);
		}
	});
}

function afficherFactures(factures)
{
	var temp = JSON.parse(factures);
	console.log(temp);
	$("#tabFactureDetail thead tr th").remove();
	$("#tabFactureDetail thead").append("<tr></tr>");

	$.each(temp[0], function(key, value) {
		$("#tabFactureDetail thead tr").append("<th>"+key+"</th>");
	});

	$.each(temp, function(key, value) {
		var element = $("#tabFactureDetail tbody").append("<tr></tr>");
		console.log(value);
		$.each(value, function(keys, values) {
			$(element).append("<td>"+values+"</td>");
		});
		
	});

	$("thead tr").append("<th></th>");
}