﻿<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php 
	include_once("action/IndexAction.php");
	$action = new IndexAction();
	$action->execute();
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
		<meta http-equiv="imagetoolbar" content="no" />
        <link href="css/style.less" rel="stylesheet/less" type="text/css" media="screen" />
		<script src="js/javascript.js"></script>
		<script src="js/jquery.js"></script>
		<script src="js/less.js" type="text/javascript"></script>
		<title><?php echo $action->translator->read("login","title")?></title>
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    </head>
    <body>
		<div class="divMainContainer">
			<div class="divLoginHeader"><h1><?php echo $action->translator->read("login","sectionTitle")?></h1></div>
			<div class="divFormLogin">
				<?php
				if(isset($_GET["error"])){
					?>
					<div class="error"><?php echo $action->translator->read("login","error")?></div>
					<?php
				}
				?>
				<form method="post" action="index.php">
					<label for="username"><?php echo $action->translator->read("login","username")?></label><input autofocus="autofocus" type="text" id="username" name="username" size="15" /><br />
					<label for="pwd"><?php echo $action->translator->read("login","pwd")?></label> <input type="password" id="pwd" name="pwd" size="15" /><br />
					<input id="boutonLogin"type="submit" value=<?php echo $action->translator->read("login","button")?> />
				</form>
				<a id="oublie" href="oublie.php"><?php echo $action->translator->read("login","forget")?></a>
				<div></div>
				<p id="choixLang"><?php echo $action->translator->read("login","labelLang")?></p><a href="index.php?lang=<?php echo $action->getOtherLang(); ?>"> <?php echo $action->translator->read("login","otherLang")?> </a>
			</div>
<?php
	include_once("partial/footer.php");