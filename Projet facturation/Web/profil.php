﻿<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                                                                *
*---------------------------------------------------------------*/
?>

<?php
	include_once("action/ProfilAction.php");

	$action = new ProfilAction();
	$action->execute();

	include_once("partial/header.php");
?>
<div id="infosProfil">
	<?php
		if(isset($_GET["temp"])){
			?>
			<div class="warning"><?php echo $action->translator->read("profil","temp")?></div>
			<?php
		}
	?>
	<ul>
		<li id ="labelUsername" ><span class="textInfo"><?php echo $action->translator->read("profil","labelUsername") ?></span> <?php echo " ".$_SESSION["username"] ?> <a href=""><?php echo $action->translator->read("profil","modifUser")?></a></li>
		<li id ="newUsernameLI">
			<form action="profil.php" method="post">
				<label for="newUsername"><?php echo $action->translator->read("profil","newUsername")?></label>
				<input autofocus="autofocus" type="text" id="newUsername" name="newUsername" size="15"/><input class="boutonConfirmer"type="submit"value=<?php echo $action->translator->read("profil","button")?> />
			</form>
		</li>
		<li><span class="textInfo"><?php echo $action->translator->read("profil","labelCourriel") ?></span><?php echo " ".$_SESSION["mail"] ?> <a href=""><?php echo $action->translator->read("profil","modifMail")?></a></li>
		<li id ="newUserMailLI">
			<form action="profil.php" method="post">
				<label for="newUserMail"><?php echo $action->translator->read("profil","newUserMail")?></label>
				<input autofocus="autofocus" type="text" id="newUserMail" name="newUserMail" size="15"/><input class="boutonConfirmer"type="submit"value=<?php echo $action->translator->read("profil","button")?> />
			</form>
		</li>
		<li <?php if(isset($_GET["temp"])){ echo "style='display:none;'";} ?>><a href=""><?php echo $action->translator->read("profil","modifPWD")?></a></li>
		<li id ="newUserPWDLI" <?php if(isset($_GET["temp"])){ echo "style='display:block;'";} ?>>
			<form action="profil.php" method="post">
				<label for="newUserPWD"><?php echo $action->translator->read("profil","newUserPWD")?></label>
				<input autofocus="autofocus" type="password" id="newUserPWD" name="newUserPWD" size="15"/>
				<div> </div>
				<label for="newUserPWD2"><?php echo $action->translator->read("profil","newUserPWD2")?></label>
				<input type="password" id="newUserPWD2" name="newUserPWD2" size="15"/><input class="boutonConfirmer"type="submit"value=<?php echo $action->translator->read("profil","button")?> />
			</form>

		</li>
	</ul>
</div>

<script>profilListener();</script>
<?php
	include_once("partial/footer.php");