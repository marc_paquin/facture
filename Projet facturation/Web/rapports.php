﻿<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                                                                *
*---------------------------------------------------------------*/
?>

<?php
	include_once("action/RapportsAction.php");

	$action = new RapportsAction();
	$action->execute();

	include_once("partial/header.php");
?>


<?php
	include_once("partial/footer.php");