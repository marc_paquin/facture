﻿<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                                                                *
*---------------------------------------------------------------*/
?>

<?php
	include_once("action/FacturationAction.php");

	$action = new FacturationAction();
	$action->execute();

	include_once("partial/header.php");
?>
	<table id="tabFacture" class="display">
		<thead>
			<tr id="headerTable">
				<th>Client</th>
				<th>Nb Factures</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$compteur = 0;
				$listeClient = $action->getClients();
				foreach($listeClient as $client) {
					if($compteur%2 === 0){
					?>
						<tr id=<?php echo $client[0]?> class="gradeA odd" onclick="showFactures(this)">
							<td><?php echo $client[2].' '.$client[1]?></td>
							<td><?php echo $client[3]?></td>
						</tr>
					<?php
					}
					else{
					?>
						<tr id=<?php echo $client[0]?> class="gradeA even" onclick="showFactures(this)">
							<td><?php echo $client[2].' '.$client[1]?></td>
							<td><?php echo $client[3]?></td>
						</tr>
					<?php
					}
					$compteur++;
				}
			?>
		</tbody>
	</table>

	<table id="tabFactureDetail" class="display">
		<thead>
		</thead>
		<tbody>
		</tbody>
	</table>

	<script>$("#tabFacture").dataTable();</script>
	<script>$("#tabFactureDetail").dataTable();</script>

<?php
	include_once("partial/footer.php");