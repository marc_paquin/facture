﻿<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                                                                *
*---------------------------------------------------------------*/
?>

<?php
	include_once("action/ClientsAction.php");

	$action = new ClientsAction();
	$action->execute();
	$compteur = 0;
	$date = date("d/m/Y");
	include_once("partial/header.php");
?>

<div id="infosClients">
	<form action="clients.php" method="post">
		<input title="<?php echo $action->translator->read("clients","tooltipAdd") ?>" class="button" id="bAdd" type="image" src="images/boutonAdd.png" name="bAdd"/>
	</form>
	<table id="tabClients" class="display">
		<thead>
			<tr>
				<th><?php echo $action->translator->read("clients","indexLastname");?></th>
				<th><?php echo $action->translator->read("clients","indexFirstname");?></th>
				<th><?php echo $action->translator->read("clients","indexAddress");?></th>
				<th><?php echo $action->translator->read("clients","indexModified");?></th>
				<th class="unusedth"></th>
				<th class="unusedth"></th>
			</tr>
		</thead>
		<tbody>
			<?php
				
				foreach($action->listeClients as $client) {
					if($compteur%2 === 0){
					?>
						<tr class="gradeA odd">
							<td><?php echo $client[2]?></td>
							<td><?php echo $client[1]?></td>
							<td><?php echo $client[3]?></td>
							<td><?php echo $client[4]?></td>
							<td onclick="modifyClient(this, <?php echo $client[0]?>)" class="button"><input title="<?php echo $action->translator->read("clients","tooltipModify") ?>" type="image" src="images/boutonModif.png" name="bModif"/></td>
							<td onclick="removeClient(this, <?php echo $client[0]?>)" class="button"><input title="<?php echo $action->translator->read("clients","tooltipRemove") ?>" type="image" src="images/boutonDelete.png" name="bModif"/></td>
						</tr>
					<?php
					}
					else{
					?>
						<tr class="gradeA even">
							<td><?php echo $client[2]?></td>
							<td><?php echo $client[1]?></td>
							<td><?php echo $client[3]?></td>
							<td><?php echo $client[4]?></td>
							<td onclick="modifyClient(this, <?php echo $client[0]?>)" class="button"><input title="<?php echo $action->translator->read("clients","tooltipModify") ?>" type="image" src="images/boutonModif.png" name="bModif"/></td>
							<td onclick="removeClient(this, <?php echo $client[0]?>)"class="button"><input title="<?php echo $action->translator->read("clients","tooltipRemove") ?>" type="image" src="images/boutonDelete.png" name="bDelete"/></td>
						</tr>
					<?php
					}
					$compteur++;
				}
				if(isset($_POST["bAdd_y"])) {
					if($compteur%2 === 0){
					?>
						<tr class="gradeA odd">
					<?php
					}
					else {
					?>
						<tr class="gradeA even">
					<?php
					}
					?>
						<form action="clients.php" method="post">
							<td><input class="inputNewClient" autofocus="autofocus" type="text" name="newLastName"/></td>
							<td><input class="inputNewClient" type="text" name="newFirstName"/></td>
							<td><input class="inputNewClient" type="text" name="newAddress"/></td>
							<td><input class="inputNewClient" type="text" name="lastUpdate" value="<?php echo $date ?>"/></td>
							<td id="tdVide"></td>
							<td class="button"><input type="image" src="images/boutonOK.png" name="bOK"/></td>
						</form>
					</tr>
					<?php
				}
			?>
		</tbody>
	</table>
</div>

<?php 
	if($_SESSION["lang"] === "fr") {
	?>
		<script>
			$('#tabClients').dataTable( {
		    "oLanguage": {
		    	"sLengthMenu": "Affiche _MENU_ résultats",
		    	"sInfo": "Affiche _START_ à _END_ de _TOTAL_ résultats",
		    	"sSearch": "Rechercher",
		    	"sInfoEmpty": "Affiche 0 à 0 de 0 résultats",
            	"sInfoFiltered": "(filtré de _MAX_ résultats)",
            	"sZeroRecords": "Aucun résultat",
		    	"oPaginate": {
		    		"sNext": "Suivant",
		    		"sPrevious": "Précédent"
		        }
		    }
		  } );
		</script>
	<?php
	}
	else {
	?>
		<script>$('#tabClients').dataTable();</script>
	<?php
	}
?>

<?php		
	include_once("partial/footer.php");