﻿<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                                                                *
*---------------------------------------------------------------*/
?>

<?php
	include_once("action/AccueilAction.php");

	$action = new AccueilAction();
	$action->execute();

	include_once("partial/header.php");
?>
	
	<?php echo $action->translator->read("home","prefix").$action->getPendingClients().$action->translator->read("home","middle").$action->getCash().$action->translator->read("home","sufix");?>	

<?php
	include_once("partial/footer.php");