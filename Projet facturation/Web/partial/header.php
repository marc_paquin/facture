﻿<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                                                                *
*---------------------------------------------------------------*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
		<meta http-equiv="imagetoolbar" content="no" />
        <link href="css/style.less" rel="stylesheet/less" type="text/css" media="screen" />
		<script src="js/javascript.js"></script>
		<script src="js/jquery.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		<script src="js/less.js" type="text/javascript"></script>
		<script src="plugins/DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
		<link href="plugins/DataTables-1.9.4/media/css/demo_page.css" rel='stylesheet' type="text/css" title="currentStyle"/>
		<link href="plugins/DataTables-1.9.4/media/css/demo_table.css" rel='stylesheet' type="text/css" title="currentStyle"/>
		<title><?php echo $action->translator->read("header","pageTitle")?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    </head>
    <body>
		<div class="divHeader">
			<div class="header-divTitre">
				<h1><?php echo $action->translator->read("header","sectionTitle")?></h1>
			</div>
			
			<div class="header-divMenu">
				<ul>
					<li><a href="accueil.php"><?php echo $action->translator->read("header","home")?></a></li>
					<li><a href="clients.php"><?php echo $action->translator->read("header","clients")?></a></li>
					<li><a href="facturation.php"><?php echo $action->translator->read("header","billing")?></a></li>
					<li><a href="rapports.php"><?php echo $action->translator->read("header","info")?></a></li>
					<li><a href="profil.php"><?php echo $action->translator->read("header","profile")?></a></li>
				</ul>
			</div>
			<div class="header-divMenu2">
				<ul>
					<li><a href="index.php?logout"><?php echo $action->translator->read("header","logout")?></a></li>
					<li><a href="<?php $url=explode("?",$_SERVER['REQUEST_URI']); echo $url[0] ?>?lang=<?php echo $action->getOtherLang(); ?>"><?php echo $action->translator->read("header","lang")?></a></li>
				</ul>
			</div>
		</div>
	
		<div class="divMainContainer">