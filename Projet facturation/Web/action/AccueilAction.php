<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php
	require_once("action/CommonAction.php");
	require_once("action/Modele/ClientsModele.php");

	class AccueilAction extends CommonAction {

		private $pendingClients;
		private $cash;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_USER);
		}
		
		protected function executeAction() {
			$this->pendingClients = ClientsModele::getPaymentPendingClients();
			$this->setCash($this->pendingClients);
			
		}

		public function setCash($clients) {
			foreach ($clients as $debt) {
				$this->cash += $debt["TOTAL"];
			}
			
		}

		public function getCash() {
			return $this->cash;
			
		}

		public function getPendingClients() {
			return sizeof($this->pendingClients);
		}
	}