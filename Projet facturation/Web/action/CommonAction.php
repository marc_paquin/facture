<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synth�se : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php
	session_start();
	require_once("action/Modele/Connection.php");
	require_once("Translator.php");
	
	abstract class CommonAction {
		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_USER = 1;
		public static $VISIBILITY_ADMIN = 2;
		private $pageVisibility;
		public $translator;
		private $lang = "en"; // langue par d�faut du site
	
		public function __construct($pageVisibility) {
			$this->pageVisibility = $pageVisibility;
		}
	
		public function execute() {			
			
			if (isset($_GET["logout"])) {
				session_unset();
				session_destroy();
				session_start();
			}

			if (!isset($_SESSION["loggedIn"])) {
				$_SESSION["loggedIn"] = CommonAction::$VISIBILITY_PUBLIC;
			}
		
			if ($_SESSION["loggedIn"] < $this->pageVisibility && $_SESSION["loggedIn"] == 0) {
				header("location:index.php");
				exit;
			}

			if ($_SESSION["loggedIn"] < $this->pageVisibility && $_SESSION["loggedIn"] == 1) {
				header("location:accueil.php");
				exit;
			}

			if (isset($_GET["lang"])) {
				$this->lang = $_GET["lang"];
				$_SESSION["lang"] = $this->lang;
			}

			if(isset($_SESSION["lang"])){
				$this->lang = $_SESSION["lang"];
			}
			
			$this->translator = new Translator($this->lang);

			$this->executeAction();

			Connection::closeConnection();
		}

		public function getOtherLang(){
			if($this->lang === "en"){
				return "fr";
			}
			else{
				return "en";
			}
		}
		
		protected abstract function executeAction();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	