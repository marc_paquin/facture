<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synth�se : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php
	require_once("action/CommonAction.php");
	require_once("action/Modele/LoginModele.php");

	class ProfilAction extends CommonAction {
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_USER);
		}
		
		protected function executeAction() {
			
			if(isset($_POST['newUsername'])) {
				LoginModele::resetUsername($_SESSION['username'],$_POST['newUsername']);
				$_SESSION['username'] = $_POST['newUsername'];
			}

			else if(isset($_POST['newUserMail'])) {
				LoginModele::resetEmail($_SESSION['username'],$_POST['newUserMail']);
				$_SESSION['mail'] = $_POST['newUserMail'];
			}

			else if(isset($_POST['newUserPWD']) && isset($_POST['newUserPWD2'])) {
				if($_POST['newUserPWD'] === $_POST['newUserPWD2']) {
					LoginModele::resetPassword($_SESSION['username'],$_POST['newUserPWD']);
					$_SESSION['pwd'] = $_POST['newUserPWD'];
				}
			}
		}
	}