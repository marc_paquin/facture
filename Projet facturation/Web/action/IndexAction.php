<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synth�se : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php
	require_once("action/CommonAction.php");
	require_once("action/Modele/Connection.php");
	require_once("action/Modele/LoginModele.php");

	class IndexAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}
		
		protected function executeAction() {
			if(isset($_POST["username"])) {
				$bd = Connection::getConnection();
				$info = LoginModele::log($_POST["username"], $_POST["pwd"]);

				$_SESSION["username"] = $info[0]["USERNAME"];
				$_SESSION["pwd"] = $info[0]["PWD"];
				$_SESSION["mail"] = $info[0]["MAIL"];
				$_SESSION["loggedIn"] = $info[0]["VISIBILITY"];

				if(!empty($info)){
					if(isset($info[0]["PWDTEMP"])){
						LoginModele::eraseTemp($_POST["username"]);
						header("location:profil.php?temp");
						exit();
					}
					else{
						header("location:accueil.php");
						exit();
					}
				}
				else
				{
					header("location:index.php?error");
					exit();
				}
			}	
		}
	}