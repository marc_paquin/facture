<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synth�se : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php
	require_once("action/CommonAction.php");
	require_once("action/Modele/ClientsModele.php");

	class ClientsAction extends CommonAction {
		public $listeClients;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_USER);
		}
		
		protected function executeAction() {
			$this->listeClients = ClientsModele::getActiveClients();

			if(isset($_POST["bOK_x"])) {
				$lastName = $_POST["newLastName"];
				$firstName = $_POST["newFirstName"];
				$address = $_POST["newAddress"];
				$lastUpdate = $_POST["lastUpdate"];
				
				ClientsModele::addClient($lastName,$firstName,$address,$lastUpdate);
				header("location:clients.php");
				exit;
			}
		}
	}