<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synthèse : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php
	require_once("action/CommonAction.php");
	require_once("action/Modele/Connection.php");
	require_once("action/Modele/LoginModele.php");

	class OublieAction extends CommonAction {
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}
		
		protected function executeAction() {
			if(isset($_POST["username"])) {
				$bd = Connection::getConnection();

				$password = $this->generatePassword();

				$info = LoginModele::getMail($_POST["username"]);
			
				$subject = $this->translator->read("forget","subject");
				$message = $this->translator->read("forget","message").$password;
				$mail = $info[0]["MAIL"];

				mail($mail,$subject,$message);
				LoginModele::setTempPassword($_POST["username"], $password);
				header("location:index.php");
				exit();
			}	
			
		}

		protected function generatePassword(){

			$password = "";
			$characters = "ABCDEFJHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for($i = 0; $i<10; $i++){
				$password .= substr($characters,rand(0,62),1);
			}

			return $password;
		}
	}