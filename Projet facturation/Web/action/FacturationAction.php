<?php
/*----------------------------------------------------------------
*                                                                *
*                   Projet synth�se : H2013                      *
*                          Fait par :                            *
*                       Justin Distaulo                          *
*                        	   &                                 *
*                        Carl Boisvert                           *
*                              :)                                *
*---------------------------------------------------------------*/
?>

<?php
	require_once("action/CommonAction.php");
	require_once("action/Modele/ClientsModele.php");


	class FacturationAction extends CommonAction {
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_USER);
		}
		
		protected function executeAction() {
			
		}

		public function getClients(){
			return ClientsModele::getClients();
		}
	}