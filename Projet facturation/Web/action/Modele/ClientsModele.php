<?php

class ClientsModele{

	public static function getActiveClients() {
		$connection = Connection::getConnection();
		
		$statement = $connection->prepare("SELECT * FROM QS_CLIENTS WHERE STATUS = 1");
		$statement->execute();
		$listeClients = array();
		
		while ($donnees = $statement->fetch()){
		    $listeClients[] = $donnees;
			
		}
		return $listeClients;	
	}

	public static function getPaymentPendingClients() {
		$connection = Connection::getConnection();
		
		$statement = $connection->prepare("SELECT * FROM QS_FACTURES WHERE PAY = 0");
		$statement->execute();
		$listeClients = array();
		while ($donnees = $statement->fetch()){
		    $listeClients[] = $donnees;
			
		}
		return $listeClients;	
	}

	public static function addClient($lastName,$firstName,$address,$lastUpdate) {
		$connection = Connection::getConnection();

		$statement = $connection->prepare("INSERT INTO QS_CLIENTS VALUES (SEQ_CLIENTS.NEXTVAL,:pFirstName,:pLastName,:pAddress,:pLastUpdate,1)");
		$statement->execute(array($firstName,$lastName,$address,$lastUpdate));
	}

	public static function deactivateClient($id) {
		$connection = Connection::getConnection();
		$statement = $connection->prepare("UPDATE QS_CLIENTS SET STATUS = 0 WHERE ID = :pId");
		$statement->execute(array($id));
	}
	public static function modifyClient($id,$nom,$prenom,$adresse,$last) {
		$connection = Connection::getConnection();
		$statement = $connection->prepare("UPDATE QS_CLIENTS SET FIRST_NAME = :pPrenom, LAST_NAME = :pNom, ADRESSE = :pAdresse, LAST_UPDATE = :pLast WHERE ID = :pId");
		$statement->execute(array($prenom,$nom,$adresse,$last,$id));
	}
	public static function getClients() {
		$connection = Connection::getConnection();
		$statement = $connection->prepare("SELECT QS_CLIENTS.ID, QS_CLIENTS.FIRST_NAME,QS_CLIENTS.LAST_NAME, COUNT(QS_FACTURES.NO_CLIENT) FROM QS_CLIENTS left join QS_FACTURES on QS_FACTURES.NO_CLIENT = QS_CLIENTS.id GROUP BY QS_CLIENTS.ID, QS_CLIENTS.FIRST_NAME, QS_CLIENTS.LAST_NAME");
		$statement->execute();
		$listeClients = array();
		while ($donnees = $statement->fetch()){
		    $listeClients[] = $donnees;
			
		}
		return $listeClients;	
	}

	public static function getFactures($id){
		$connection = Connection::getConnection();
		$statement = $connection->prepare("SELECT * FROM QS_FACTURES WHERE NO_CLIENT = :pId");
		$statement->execute(array($id));
		$factures = array();
		while ($donnees = $statement->fetch(PDO::FETCH_ASSOC)){
		    $factures[] = $donnees;
		}
		return $factures;
	}

}