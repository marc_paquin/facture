<?php


	class LoginModele{

		public static function log($username, $password) {
			$connection = Connection::getConnection();

			$password = sha1($password."saltyllama");
			
			$statement = $connection->prepare("SELECT * FROM QS_USERS WHERE USERNAME = :pUsername AND (PWD = :pPassword OR PWDTEMP = :pPassword)");
			$statement->execute(array($username, $password));
			$userInfo = null;
			
			while ($donnees = $statement->fetch()){
			    $userInfo[] = $donnees;
				
			}
			return $userInfo;	
		}

		public static function setTempPassword($username, $password){

			$connection = Connection::getConnection();

			$password = sha1($password."saltyllama");
			
			$statement = $connection->prepare("UPDATE QS_USERS SET PWDTEMP = :pPassword WHERE USERNAME = :pUsername");
			$statement->execute(array($password,$username));
		}

		public static function getMail($username){

			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("SELECT MAIL FROM QS_USERS WHERE USERNAME = :pUsername");
			$statement->execute(array($username));
			$userInfo = null;
			
			while ($donnees = $statement->fetch()){
			    $userInfo[] = $donnees;
			}
			return $userInfo;	
		}

		public static function eraseTemp($username){

			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("UPDATE QS_USERS SET PWDTEMP = NULL WHERE USERNAME = :pUsername");
			$statement->execute(array($username));
		}

		public static function resetPassword($username, $password){

			$connection = Connection::getConnection();

			$password = sha1($password."saltyllama");
			
			$statement = $connection->prepare("UPDATE QS_USERS SET PWD = :pPassword WHERE USERNAME = :pUsername");
			$statement->execute(array($password,$username));
		}

		public static function resetUsername($username, $newUsername){

			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("UPDATE QS_USERS SET USERNAME = :pNewUsername WHERE USERNAME = :pUsername");
			$statement->execute(array($newUsername,$username));
		}

		public static function resetEmail($username, $email){

			$connection = Connection::getConnection();
			
			$statement = $connection->prepare("UPDATE QS_USERS SET MAIL = :pEmail WHERE USERNAME = :pUsername");
			$statement->execute(array($email,$username));
		}
	}