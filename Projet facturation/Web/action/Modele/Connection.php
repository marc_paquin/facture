<?php
	require_once("config.php");

	class Connection
	{
		private static $connection;
	
		public static function getConnection() {
			if (!isset(Connection::$connection)) {
				Connection::$connection = new PDO(HOST,USER, PASS);
			}
			
			return Connection::$connection;
		}
	
	
		public static function closeConnection() {
			if (isset(Connection::$connection)) {
				Connection::$connection = null;
			}
		}
	}